using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    // �������� �������� �����������
    [SerializeField] GameObject[] enemyPrefabs;
    // �������� ��������� �����������
    private List<GameObject> createdEnemies = new List<GameObject>();

    // ����� ��� ������ �����������
    const float spawnTimeMin = 0.5f;
    const float spawnTimeMax = 1.5f;

    // ���������� ��� ������ �����������
    // �����, ����������� � ������ ����� ��������������
    int[] positionSpawnLines = { -3, 0, 3 };
    
    private void OnEnable()
    {
        float randomNum = Random.Range(spawnTimeMin, spawnTimeMax);
        StartCoroutine(Spawner(randomNum));
    }
    IEnumerator Spawner(float randomNum)
    {
        yield return new WaitForSeconds(randomNum);
        // ��������������� ���������� ������� ������
        randomNum = Random.Range(spawnTimeMin, spawnTimeMax);
        // ����� �� ��������� �����
        int randomLine = positionSpawnLines[Random.Range(0, positionSpawnLines.Length)];
        
        GameObject enemyPrefab = enemyPrefabs[Random.Range(0, enemyPrefabs.Length)];
        GameObject enemy = Instantiate(enemyPrefab, new Vector3 (randomLine, transform.position.y, transform.position.z), transform.rotation);
        createdEnemies.Add(enemy);

        if (createdEnemies.Count > 5)
            DeleteEnemy();

        StartCoroutine(Spawner(randomNum));
    }
    void DeleteEnemy()
    {
        Destroy(createdEnemies[0]);
        createdEnemies.RemoveAt(0);
    }

}
