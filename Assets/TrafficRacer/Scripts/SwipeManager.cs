using UnityEngine;

namespace TR2
{
    public class SwipeManager : MonoBehaviour
    {
        // ����������� ������
        protected static bool swipeLeft, swipeRight;
        // ����������� ������
        private bool isDraging = false;
        private Vector2 startTouch, swipeDelta;
        protected void GetSwipeData()
        {
            swipeLeft = swipeRight = false;
            if (Input.GetMouseButtonDown(0))
            {
                isDraging = true;
                startTouch = Input.mousePosition;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                isDraging = false;
                Reset();
            }

            if (Input.touches.Length > 0)
            {
                if (Input.touches[0].phase == TouchPhase.Began)
                {
                    isDraging = true;
                    startTouch = Input.touches[0].position;
                }
                else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
                {
                    isDraging = false;
                    Reset();
                }
            }

            //���������� ���������
            swipeDelta = Vector2.zero;
            if (isDraging)
            {
                if (Input.touches.Length < 0)
                    swipeDelta = Input.touches[0].position - startTouch;
                else if (Input.GetMouseButton(0))
                    swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }

            //�������� �� ������������ ����������
            if (swipeDelta.magnitude > 100)
            {
                SetDir();
                Reset();
            }

        }
        private void SetDir()
        {
            //����������� �����������
            float x = swipeDelta.x;
            if (x < 0)
                swipeLeft = true;
            else
                swipeRight = true;
        }
        private void Reset()
        {
            startTouch = swipeDelta = Vector2.zero;
            isDraging = false;
        }
    }
}