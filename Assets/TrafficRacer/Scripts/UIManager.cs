using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// ��� �������� ����� ������������ ���������
namespace TR2
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private GameObject main_menu, game_menu, game_over, select_menu;
        [SerializeField] private GameObject playerSwipe_controller, selectionMenuSwipe_controller, player_movement, enemy_spawner;
        // ��� ����������� ��������� �� ������
        [SerializeField] private Text distance_text;
        [SerializeField] private Transform playerTransform;
        // ��� ����, ����� ���� �� ������������, ���� �� ��������� � ����
        double afk_score = 0;
        private void Start()
        {
            GameManager.singeton.gameStatus = GameStatus.MENU;                              
        }

        private void Update()
        {
            // ���������� ����� ��� ������ ������� ������
            if (GameManager.singeton.gameStatus == GameStatus.MENU)
                afk_score = playerTransform.position.z;
            distance_text.text = $"Score: {(int)(playerTransform.position.z - afk_score) / 10}";

            // ���������� ����������� ������� ������ ��� ����
            if (GameManager.singeton.gameStatus != GameStatus.PLAY)
                playerSwipe_controller.SetActive(false);

            if (!select_menu.activeSelf)
            {
                selectionMenuSwipe_controller.SetActive(false);
                player_movement.SetActive(true);
            }
            if (GameManager.singeton.gameStatus != GameStatus.PLAY)
                enemy_spawner.SetActive(false);

            // ���� ���������
            if (GameManager.singeton.gameStatus == GameStatus.LOSE)
                GameOver();
        }

        // ������ "play"
        public void PlayButton()
        {
            main_menu.SetActive(false);
            game_menu.SetActive(true);
            GameManager.singeton.gameStatus = GameStatus.PLAY;

            playerSwipe_controller.SetActive(true);
            enemy_spawner.SetActive(true);
        }

        // ������ "restart"
        public void RestartButton()
        {
            // ������������ �����
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        // ������ "select" � ������� ����
        public void SelectButton()
        {
            main_menu.SetActive(false);
            select_menu.SetActive(true);
            selectionMenuSwipe_controller.SetActive(true);
            player_movement.SetActive(false);
        }

        // ������ "select" � ���� ������ ������
        public void SelectCarButton()
        {
            main_menu.SetActive(true);
            select_menu.SetActive(false);
        }

        // ������ "����� �� ���� ������"
        public void BackToHomeButton()
        {
            main_menu.SetActive(true);
            // ��� ����, ����� ����� ������ ��������� ������ ��� ������ ������ select
            int selectedCar = GameManager.singeton.currentCarIndex;
            select_menu.SetActive(false);
            GameManager.singeton.currentCarIndex = selectedCar;
        }
        public void ExitGameButton()
        {
            Application.Quit();
        }
        public void GameOver()
        {
            game_over.SetActive(true);
        }
    }
}