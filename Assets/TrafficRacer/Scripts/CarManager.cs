using System.Collections.Generic;
using UnityEngine;

namespace TR2
{
    public class CarManager : SelectionMenuSwipeController
    {
        [SerializeField] GameObject[] carPrefabs;
        private int selectedCar;
        public List<GameObject> cars = new List<GameObject>();

        private void OnEnable()
        {
            selectedCar = GameManager.singeton.currentCarIndex;
            cars.Add(Instantiate(carPrefabs[selectedCar], transform));
        }
        private void OnDisable() 
        {
            Destroy(cars[0]);
            cars.RemoveAt(0);

            GameManager.singeton.currentCarIndex = selectedCar;
        }

        void Update()
        {
            if (SelectionMenuSwipeController.swipeLeft)
                selectedCar++;
            else if (SelectionMenuSwipeController.swipeRight)
                selectedCar--;
            else
                return;

            // ����������� �� ������������ � ����������� ����� ������
            selectedCar = Mathf.Clamp(selectedCar, 0, carPrefabs.Length - 1);
            Destroy(cars[0]);
            cars.RemoveAt(0);
            cars.Add(Instantiate(carPrefabs[selectedCar], transform));
        }
    }
}