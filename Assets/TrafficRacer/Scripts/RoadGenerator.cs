using System.Collections.Generic;
using UnityEngine;
public class RoadGenerator : MonoBehaviour
{

    [SerializeField] private GameObject groundPrefab;
    [SerializeField] private Transform playerTransform;

    // ����� 1 ���������
    private const int prefabLenght = 10;
    // � ��� ��� ���� 5 ��������
    private const int prefabCount = 5;
    private int spawnPos = prefabLenght * prefabCount;
    // ��� �������� ��� ���������� ��������
    private List<GameObject> prefabs = new List<GameObject>();
    // �������, ����� ������ ��������� �� ���������
    int count = 0;
    void Update()
    {
        if (playerTransform.position.z > spawnPos - (prefabCount * prefabLenght))
        {
            SpawnPrefab();
            if (count > prefabCount)
            {
                DeletePrefab();
            }
            count++;
        }
    }

    void SpawnPrefab()
    {
        GameObject tile = Instantiate(groundPrefab, transform.forward * spawnPos, transform.rotation);
        prefabs.Add(tile);
        spawnPos += prefabLenght;
    }

    void DeletePrefab()
    {
        Destroy(prefabs[0]);
        prefabs.RemoveAt(0);
    }
}
