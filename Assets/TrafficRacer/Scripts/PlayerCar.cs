using TR2;
using UnityEngine;

public class PlayerCar : MonoBehaviour
{
    [SerializeField] public GameObject[] carsPrefabs;
    GameObject carSpawned;
    // Start is called before the first frame update
    private void Start()
    {
        carSpawned = Instantiate(carsPrefabs[GameManager.singeton.currentCarIndex], transform);
    }
    void OnEnable()
    {
        carSpawned = Instantiate(carsPrefabs[GameManager.singeton.currentCarIndex], transform);
    }
    private void OnDisable()
    {
        Destroy(carSpawned);
    }
}
