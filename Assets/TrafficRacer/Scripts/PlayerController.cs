using UnityEngine;

namespace TR2
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : PlayerSwipeController
    {
        CharacterController characterController;
        private Vector3 dir;

        private float gameSpeed;
        private const float mainMenuSpeed = 20;
        private const float InGameSpeed = 30;
        // ���������
        private float a;
        // ����� ��� �����
        int lineToMove = 1;
        // ��� ��������� � ����� �����
        int lineCenter = 3;

        void Start()
        {
            characterController = GetComponent<CharacterController>();
            a = (InGameSpeed - mainMenuSpeed) / InGameSpeed;
        }

        private void Update()
        {
            CheckSwipe();
            DoSwipe();
        }
        void CheckSwipe()
        {
            // �������� � ����� ������� ���� �����
            if (PlayerSwipeController.swipeRight)
                lineToMove++;
            else if (PlayerSwipeController.swipeLeft)
                lineToMove--;
            // ����� ����� 3, ������� ����� �����������
            lineToMove = Mathf.Clamp(lineToMove, 0, 2);
        }
        void DoSwipe()
        {
            // ���������� ������ �� ����� �����
            Vector3 nextPos = transform.position.z * transform.forward + transform.position.y * transform.up;

            if (lineToMove == 0)
                nextPos += Vector3.left * lineCenter;
            else if (lineToMove == 2)
                nextPos += Vector3.right * lineCenter;

            if (transform.position == nextPos)
                return;

            Vector3 diff = nextPos - transform.position;
            Vector3 moveDir = diff.normalized * 25 * Time.deltaTime;
            if (moveDir.sqrMagnitude < diff.sqrMagnitude)
                characterController.Move(moveDir);
            else
                characterController.Move(diff);
        }

        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            // ���� �������� �����������, �� ���� �������������
            if (hit.gameObject.tag == "Enemy")
            {
                gameSpeed = 0;
                a = 0;
                GameManager.singeton.gameStatus = GameStatus.LOSE;
            }
        }

        void FixedUpdate()
        {
            if (GameManager.singeton.gameStatus != GameStatus.LOSE)
            {
                // ���� ������ ������, �� ��������� ����������� ��������
                if (GameManager.singeton.gameStatus == GameStatus.PLAY)
                {
                    gameSpeed += 0.02f;
                    if (gameSpeed < InGameSpeed)
                        gameSpeed += a;
                }
                else
                {
                    // ��������, ���� ��������� � ����            
                    gameSpeed = mainMenuSpeed;
                }
                dir.z = gameSpeed;
                characterController.Move(dir * Time.deltaTime);
            }
        }
    }
}
