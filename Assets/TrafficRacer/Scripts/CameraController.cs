using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform player;
    private Vector3 dispos;
    private void Awake()
    {
        dispos = transform.position - player.position;
    }

    private void FixedUpdate() 
    {
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y, dispos.z + player.position.z);
        transform.position = newPos;
    }
}
